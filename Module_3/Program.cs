using System;
using System.Collections;
using System.Collections.Generic;

namespace Module_3
{
    class Program
    {   
        static void Main(string[] args)
        {
            int b;
            Task1 task1 = new Task1();
            Task2 task2 = new Task2();
            Task3 task3 = new Task3();
            task1.ParseAndValidateIntegerNumber("132");
            task1.Multiplication(8, 8);
            task2.TryParseNaturalNumber("153", out b);
            task3.RemoveDigitFromNumber(12345678, 8);
            /// Use this method to implement tasks
        }
    }
    
      
        public class Task1
        {
            /// <summary>
            /// Use this method to parse and validate user input
            /// Throw ArgumentException if user input is invalid
            /// </summary>
            /// <param name="source"></param>
            /// <returns></returns>
            public int ParseAndValidateIntegerNumber(string source)
            {
                int res = 0;
                bool tryparse;
                tryparse = int.TryParse(source, out res);
                Console.WriteLine(res);
                return res;
            }

            public int Multiplication(int num1, int num2)
            {
                int res = 0;
                for (int i = 1; i <= num2; i++)
                {
                    res += num1;
                }
                Console.WriteLine(res);
                return res;
            }
        }

        public class Task2
        {
            /// <summary>
            /// Use this method to parse and validate user input
            /// </summary>
            /// <param name="source"></param>
            /// <returns></returns>
            public bool TryParseNaturalNumber(string input, out int result)
            {
                bool res;
                res = int.TryParse(input, out result);
                Console.WriteLine(result);
                if (result < 0)
                {
                    Console.WriteLine("Попробуйте снова.");
                }
                else
                {
                    if (result % 1 == 0)
                    {
                        GetEvenNumbers(result);
                    }
                    else
                    {
                        Console.WriteLine("Попробуйте снова.");
                    }
                }
                return res;
            }

            public List<int> GetEvenNumbers(int naturalNumber)
            {
                List<int> res = new List<int>();
                for (int i = 0; i < naturalNumber; i++)
                {
                    if (i % 2 == 0)
                    {
                        res.Add(i);
                    }
                }
                for (int i = 0; i < res.Count; i++)
                {
                    Console.WriteLine(res[i]);
                }
                return res;
            }
        }

        public class Task3
        {
            /// <summary>
            /// Use this method to parse and validate user input
            /// </summary>
            /// <param name="source"></param>
            /// <returns></returns>
            public bool TryParseNaturalNumber(string input, out int result)
            {
                bool res;
                res = int.TryParse(input, out result);
                if (result % 1 == 0)
                {

                }
                else
                {
                    Console.WriteLine("Попробуйте снова.");
                }
                return res;
            }

            public string RemoveDigitFromNumber(int source, int digitToRemove)
            {
                string num1 = "";
                string num2 = "";
                string res = "";
                num1 = source.ToString();
                num2 = digitToRemove.ToString();
                res = num1.Replace(num2, "");
                Console.WriteLine(res);
                return res;
            }
        }
}
